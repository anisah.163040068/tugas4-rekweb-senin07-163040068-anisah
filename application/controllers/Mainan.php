<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// ketikan source yang ada di modul
		$this->API = "http://localhost/rest_server";		
	}

	public function index()
	{
	// ketikan source yang ada di modul
		$data['judul'] = 'Rest API';
		// Sedot data JSON dari localhost/rest_server/mainan
		$data['mainan'] = json_decode($this->curl->simple_get($this->API . '/mainan/')); 
		$data['main'] = $data['mainan'];
		$data['content'] = 'mainan/mainan';

		$this->load->view('template/template', $data);

	}

	public function detail($id)
	{
		$data['judul'] = 'Rest API';
		//$params = array('id_anggota' => $this->uri->segment(3));
		$data['id'] = 'id_mainan';
		$data['mainan'] = json_decode($this->curl->simple_get($this->API . '/mainan', array('id_mainan' => $id))); //array("id" => $id)
		$data['main'] = $data['mainan'];
		$data['content'] = 'mainan/detail';

		$this->load->view('template/template', $data); 	
	}

	public function search()
	{a
		// ketikan source yang ada di modul 
		//$search = urldecode('informatika');
		$input = $this->input->get('keyword');
		$search = urldecode($input);
		$data['judul'] = 'Rest API';
		$data['mainan'] = json_decode($this->curl->simple_get($this->API . 'mainan/search/?cari=' . urldecode($search)), TRUE);
		$data['main'] = $data['mainan'];
		$data['content'] = 'mainan/mainan';

		$this->load->view('template/template', $data);
	}

	public function create()
	{
		// ketikan source yang ada di modul
		$harga_mainan = $this->put('harga_mainan');
	    $stok_mainan = $this->put('stok_mainan');
	    $deskripsi = $this->put('deskripsi');
	    $kategori = $this->put('kategori');
	    $nama_mainan = $this->put('nama_mainan');
	    $id_mainan = $this->put('id_mainan');

		$data = array(
			'nama_mainan' => $nama_mainan,
	        'kategori' =>$kategori,
	        'deskripsi' => $deskripsi,
	        'stok_mainan' => $stok_mainan,
	        'harga_mainan' => $harga_mainan,
			);

		$this->curl->simple_post($this->API . '/mainan/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" => TRUE));
	}

	public function edit()
	{
		// ketikan source yang ada di modul
		$id = $this->input->post("id_mainan");
		$params = array('id_mainan' => $id);
		$data['mainan'] = json_decode($this->curl->simple_get($this->API . '/mainan/', $params));
		$mahasiswa = $data['mainan'];
		$json = json_encode(array("status" => 200, "main" =>$mainan));
		echo $json;
	}

	public function update()
	{
		// ketikan source yang ada di modul
		$nama_mainan = $this->input->post('namamainanUpdate');
		$kategori = $this->input->post('kategoriUpdate');
		$deskripsi = $this->input->post('deskripsiUpdate');
		$stok_mainan = $this->input->post('stokmainanUpdate');
		$harga_mainan = $this->input->post('hargamainanUpdate');
		$id_mainan = $this->input->post("$id_mainan");
		
	    

		$data = array(
			'id_mainan' => $id_mainan,
			'nama_mainan' => $nama_mainan,
	        'kategori' =>$kategori,
	        'deskripsi' => $deskripsi,
	        'stok_mainan' => $stok_mainan,
	        'harga_mainan' => $harga_mainan,
			);

		$this->curl->simple_put($this->API . '/mainan/', $data, array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" => TRUE));

	}

	public function delete()
	{
		// ketikan source yang ada di modul
		$id = $this->input->post('id_mainan');
		json_decode($this->curl->simple_delete($this->API . '/mainan/', array('id' => $id),
		array(CURLOPT_BUFFERSIZE => 10 )));
		json_encode(array("status" => TRUE));
	}
}
