<?php

class Mainan_Model extends CI_model{
    public function __construct()
	{
		parent::__construct();
	}

    public function getData($id = null){
        // ketikan source code yang ada di modul
        $this->db->select("*");
        $this->db->from("mainan");

        if ($id == null) {
            $this->db->order_by('id_mainan', 'asc');
        } else {
            $this->db->where('id_mainan', $id);
        }
        return $this->db->get();
    }

    public function cari($cari)
    {
        // ketikan source code yang ada di modul
        $this->db->select("*");
        $this->db->from("mainan");
        $this->db->like('nama_mainan', $cari);
        $this->db->or_like('kategori', $cari);
        $this->db->or_like('deskripsi', $cari);
        $this->db->or_like('stok_mainan', $cari);
        $this->db->or_like('harga_mainan', $cari);
        return $this->db->get();
    }

    public function insert($data){
       // ketikan source code yang ada di modul
        $this->db->insert('mainan', $data);
        return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
        // ketikan source code yang ada di modul
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();
    }
    
    public function delete($table, $par, $var){
       // ketikan source code yang ada di modul
        $this->db->where($par, $var);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }  
    
}