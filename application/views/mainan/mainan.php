<!-- Modal Tambah -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Mainan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" id="formTambah" method="post">
					<div class="form-group">
						<label for="nama">Nama Mainan</label>
						<input type="text" class="form-control" name="nama" id="nama"
							   placeholder="Masukan Nama Mainan">
					</div>
					<div class="form-group">
						<label for="kategori">Kategori</label>
						<input type="text" class="form-control" id="kategori" 
							   name="kategori" placeholder="Masukan Kategori">
					</div>
					<div class="form-group">
						<label for="deskripsi">Deskripsi</label>
						<input type="text" class="form-control" id="deskripsi"
							   name="deskripsi" placeholder="Masukan Deskripsi">
					</div>
					<div class="form-group">
						<label for="stok">Stok Mainan</label>
						<input type="number" class="form-control" name="stok" id="stok_mainan"
							   placeholder="Masukan Stok Mainan">
					</div>
					<div class="form-group">
						<label for="harga">Harga Mainan</label>
						<input type="number" class="form-control" id="harga_mainan" 
							   name="harga" placeholder="Masukan Harga Mainan">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" id="submitAdd" class="btn btn-primary" name="add">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Update -->
<div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel1">Update Mainan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" id="formUpdate" method="post">
					<input type="hidden" name="id" id="id_mhs">
					<div class="form-group">
						<label for="namamainaUpdate">Nama Mainan</label>
						<input type="text" value="" class="form-control" name="namamainanUpdate" id="namamainanUpdate"
							   placeholder="Masukan Nama Mainan">
					</div>
					<div class="form-group">
						<label for="kategoriUpdate">Nama kategori</label>
						<input type="text" value="" class="form-control" name="kategoriUpdate" id="kategoriUpdate"
							   placeholder="Masukan Nama Kategori">
					</div>
					<div class="form-group">
						<label for="deskripsiUpdate">Deskripsi</label>
						<input type="text" value="" class="form-control" name="deskripsiUpdate" id="deskripsiUpdate"
							   placeholder="Masukan Deskripsi">
					</div>
					<div class="form-group">
						<label for="stokmainanUpdate">Stok Mainan</label>
						<input type="number" class="form-control" id="stokmainanUpdate" 
							   name="stokmainanUpdate" placeholder="Masukan Stok Mainan">
					</div>
					<div class="form-group">
						<label for="hargamainanUpdate">Harga Mainan</label>
						<input type="number" class="form-control" id="hargamainanUpdate" 
							   name="hargamainanUpdate" placeholder="Masukan Harga Mainan">
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="add" id="submitUpdate">Simpan</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3 style="text-align: center;">Daftar Mainan</h3>
			<button class="btn btn-success btn-sm" id="buttonAdd" data-toggle="modal" data-target="#exampleModal">
				Tambah
			</button>
			<div class="col-3" style="float: right; position: relative; left: 15px">
				<form action="<?= site_url('/mainan/search/') ?>" method="get">
					<div class="form-group">
						<div class="input-group">
							<input type="text" name="keyword" class="form-control" placeholder="Search...">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-primary"
										style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">Search
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- ketikan source yang ada di modul -->
			<table class="table table-striped table-bordered" id="mytable" style="margin-top: 20px">
				<thead class="thead-dark">
					<tr>
						<th scope="col" style="text-align: center;">No</th>
						<th scope="col" style="text-align: center;">Nama Mainan</th>
						<th scope="col" style="text-align: center;">Kategori</th>
						<th scope="col" style="text-align: center;">Deskripsi</th>
						<th scope="col" style="text-align: center;">Stok Mainan</th>
						<th scope="col" style="text-align: center;">Harga Mainan</th>
						<th scope="col" style="text-align: center;">Action</th>
					</tr>
				</thead>
				<?php if (empty($main)): ?>
				<tr>
					<td colspan="6" style="text-align: center;">Maaf data yang anda cari tidak ditemukan</td>
				</tr>
			<?php else : ?>
			<?php $no = 1 ?>
			<?php foreach ($main as $key) : ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $key->nama_mainan ?></td>
				<td><?= $key->kategori ?></td>
				
				<td><?= $key->deskripsi ?></td>
				<td style="text-align: center;">
					<a href="<?= site_url('mainan/detail/' . $key->id_mainan) ?>" class="btn btn-sm btn-primary">Detail</a>
					<a href="" class="btn btn-sm btn-success update" id="<?= $key->npm ?>">Update</a>
					<a href="" id="<?= $key->npm ?>" class="btn btn-sm btn-danger delete">Delete</a>
				</td>
			</tr>
		<?php endforeach ?>
	<?php endif ?>
			</table>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
<script>
	$(document).ready(function () {
		
		$("#submitAdd").click(function (e) {
			e.preventDefault();
			$.ajax({
				url: "<?= site_url('mainan/create') ?>",
				type: "POST",
				dataType; "JSON",
				data: $("#formTambah").serialize(),
				success: function () {
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "success",
						buttons: false,
					});
					setTimeout(function(){
						location.reload();
					}, 2000 );
				},
				error: function(xhr, status, error){
					alert(status + " : " + error);
				}
			});
		});

		$(".update").click(function (e) {
			// ketikan source yang ada di modul
			e.preventDefault();
			$("#modalUpdate").modal('show');
			var id = $(this).attr("id");
			$.ajax({
				url: "<?php echo site_url('mainan/edit/') ?>",
				type: "POST",
				data: "id=" + id,
				dataType: 'json',
				success: function(data){
					if(data.status == 200){
						$("#id_mainan").val(data.main[0].id);
						$("#namamainanUpdate").val(data.main[0].nama_mainan);
						$("#kategoriUpdate").val(data.main[0].kategori);
						$("#deskripsiUpdate").val(data.main[0].deskripsi);
						$("#stokmainanUpdate").val(data.main[0].stok_mainan);
						$("#hargamainanUpdate").val(data.main[0].harga_mainan);
					}
				},
				error: function (xhr, status, error){
					alert(status + " : " + error);
				}
			});
		});

		$("#submitUpdate").click(function (e) {
			// ketikan source yang ada di modul
			e.preventDefault();
			$.ajax({
				url: "<?= site_url('mainan/update') ?>",
				type: "POST",
				data: $("#formUpdate").serialize(),
				dataType: "JSON",
				success: function(){
					$('#exampleModal').modal('hide');
					swal({
						title: "Success",
						text: "Data berhasil disimpan",
						icon: "Success",
						buttons: false,
					});
					setTimeout(function(){
						location.reload();
					}, 2000);
				},
				error: function (xhr, status, error){
					alert(status + " : " + error);
				}
			});
		});

		$(".delete").click(function (e) {
			// ketikan source yang ada di modul
			e.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Apakah anda yakin ingin menghapus ?",
				text: "Data yang terhapus tidak bisa dipulihkan",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete){
					$.ajax({
						url: "<?= site_url('mainan/delete') ?>",
						type: "POST",
						data: "id=" + id,
						dataType: "JSON",
						success: function(data){
							if(data.status == true){
								swal({
									title: "Success",
									text: "Data berhasil dihapus",
									icon: "success",
									buttons: false,
								});
								setTimeout(function(){
									location.reload();
								}, 2000);
							}
						},
						error: function(xhr, status, error){
							alert(status + " : " + error);
						}
					});
				} else{
					swal("Batal menghapus data");
				}
			});
		});
	});
</script>


