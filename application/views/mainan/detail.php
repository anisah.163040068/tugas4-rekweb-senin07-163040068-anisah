<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><?= $main[0]->id_mainan ?></h5>
			<h6 class="card-subtitle mb-2 text-muted"><?= $main[0]->id_mainan ?></h6>
			<p class="card-text"><?= $main[0]->nama_mainan ?></p>
			<p class="card-text"><?= $main[0]->kategori ?></p>
			<a href="<?= site_url('mainan') ?>" class="card-link">Kembali</a>
		</div>
	</div>
</div>
