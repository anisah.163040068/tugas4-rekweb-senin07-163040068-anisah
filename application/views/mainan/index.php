<div class="container">
    <div class="jumbotron jumbotron-fluid mt-4">
    <div class="container">
        <h1 class="display-4">Welcome</h1>
        <p class="lead"></p>
    </div>
</div>
<div class="row" id="mainan">
<?php foreach($mainan as $main ) :?>
    <div class="col-sm-4 mt-2">
              <div class="card">
                <img class="card-img-top" src="<?= $main['gambar'];?>" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"><?= $main['nama_mainan'];?></h4>
                  <p class="card-text"><?= $main['deskripsi'];?></p>
                  <h3 class="card-title">Rp.<?= $main['harga_mainan'];?></h3>
                  <a href="#" class="btn btn-primary">Pesan Sekarang!</a>
                </div>
              </div>
            </div>
<?php endforeach;?>
</div>
</div>
